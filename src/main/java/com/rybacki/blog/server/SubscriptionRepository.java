package com.rybacki.blog.server;

import com.rybacki.blog.server.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

    List<Subscription> findByAuthorUserNameEquals(String authorUserName);
}
