package com.rybacki.blog.server;

import com.rybacki.blog.server.model.BlogPost;
import com.rybacki.blog.server.model.BlogPostResponse;
import com.rybacki.blog.server.model.Subscription;
import com.rybacki.blog.server.model.SubscriptionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlogService {

    private BlogRepository blogRepository;
    private SubscriptionRepository subscriptionRepository;
    private SubscriptionService subscriptionService;

    @Autowired
    public BlogService(BlogRepository blogRepository, SubscriptionRepository subscriptionRepository,
                       SubscriptionService subscriptionService) {
        this.blogRepository = blogRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.subscriptionService = subscriptionService;
    }

    BlogPost findPostById(Long postId) {
        return blogRepository.findById(postId).orElseThrow(
          () -> new IllegalArgumentException("Post with given id: " + postId + " is not defined."));
    }

    BlogPostResponse savePost(BlogPost blogPost) {
        BlogPost post = blogRepository.save(blogPost);
        subscriptionService.sendSubscriptionAsync(post.getAuthorUserName());
        return new BlogPostResponse(post.getPostId());
    }

    SubscriptionResponse saveSubscription(Subscription subscription) {
        return new SubscriptionResponse(subscriptionRepository.save(subscription).getSubscriptionId());
    }

    void deleteSubscriptionById(Long subscriptionId) {
        subscriptionRepository.deleteById(subscriptionId);
    }
}
