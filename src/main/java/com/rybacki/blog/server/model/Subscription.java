package com.rybacki.blog.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Subscription {

    @Id
    @GeneratedValue
    private Long subscriptionId;
    private String userId;
    private String authorUserName;

    public Subscription() {
    }

    public Subscription(String userId, String authorUserName) {
        this.userId = userId;
        this.authorUserName = authorUserName;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public String getUserId() {
        return userId;
    }

    public String getAuthorUserName() {
        return authorUserName;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "subscriptionId=" + subscriptionId +
                ", userId='" + userId + '\'' +
                ", authorUserName='" + authorUserName + '\'' +
                '}';
    }
}
