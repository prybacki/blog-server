package com.rybacki.blog.server.model;

public class BlogPostResponse {

    private Long postId;

    public BlogPostResponse() {
    }

    public BlogPostResponse(Long postId) {
        this.postId = postId;
    }

    public Long getPostId() {
        return postId;
    }

    @Override
    public String toString() {
        return "BlogPostResponse{" +
                "postId=" + postId +
                '}';
    }
}

