package com.rybacki.blog.server.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.List;

@Entity
public class BlogPost {

    @Id
    @GeneratedValue
    private Long postId;
    private String content;
    private String subject;

    @ElementCollection
    private List<String> tags;

    @DateTimeFormat()
    private LocalDate modificationDate;
    private String authorName;
    private String authorUserName;

    public BlogPost() {
    }

    public BlogPost(String content, String subject, List<String> tags, LocalDate modificationDate,
                    String authorName, String authorUserName) {
        this.content = content;
        this.subject = subject;
        this.tags = tags;
        this.modificationDate = modificationDate;
        this.authorName = authorName;
        this.authorUserName = authorUserName;
    }

    public Long getPostId() {
        return postId;
    }

    public String getContent() {
        return content;
    }

    public String getSubject() {
        return subject;
    }

    public List<String> getTags() {
        return tags;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorUserName() {
        return authorUserName;
    }

    @Override
    public String toString() {
        return "BlogPost{" +
                "postId=" + postId +
                ", content='" + content + '\'' +
                ", subject='" + subject + '\'' +
                ", tags=" + tags +
                ", modificationDate=" + modificationDate +
                ", authorName='" + authorName + '\'' +
                ", authorUserName='" + authorUserName + '\'' +
                '}';
    }
}
