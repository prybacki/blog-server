package com.rybacki.blog.server.model;

public class SubscriptionResponse {

    private Long subscriptionId;

    public SubscriptionResponse() {
    }

    public SubscriptionResponse(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    @Override
    public String toString() {
        return "SubscriptionResponse{" +
                "subscriptionId=" + subscriptionId +
                '}';
    }
}

