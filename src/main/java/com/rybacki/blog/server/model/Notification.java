package com.rybacki.blog.server.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rybacki.blog.server.SubscriptionService;

public class Notification {

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("message")
    private String message;

    public Notification(String userId, String message) {
        this.userId = userId;
        this.message = message;
    }

    public Notification() {
    }

    public Notification(Subscription subscription) {
        this.userId = subscription.getUserId();
        this.message = SubscriptionService.NOTIFICATION_SAMPLE_MESSAGE;
    }

    public String getUserId() {
        return userId;
    }

    public String getMessage() {
        return message;
    }
}
