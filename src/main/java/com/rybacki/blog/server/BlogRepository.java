package com.rybacki.blog.server;

import com.rybacki.blog.server.model.BlogPost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogRepository extends JpaRepository<BlogPost, Long> {

    boolean existsByAuthorUserName(String authorUserName);
}
