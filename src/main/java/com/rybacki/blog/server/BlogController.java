package com.rybacki.blog.server;

import com.rybacki.blog.server.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/")
@RestController
public class BlogController {

    private BlogService blogService;

    @Autowired
    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }

    @GetMapping("posts/{id}")
    public BlogPost retrievePost(@PathVariable("id") Long postId) {
        return blogService.findPostById(postId);
    }

    @PostMapping("posts")
    public BlogPostResponse createPost(@RequestBody BlogPost blogPost) {
        return blogService.savePost(blogPost);
    }

    @PostMapping("subscriptions")
    public SubscriptionResponse createSubscription(@RequestBody Subscription subscription) {
        return blogService.saveSubscription(subscription);
    }

    @DeleteMapping("subscriptions/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteSubscription(@PathVariable("id") Long subscriptionId) {
        blogService.deleteSubscriptionById(subscriptionId);
    }

    @RequestMapping(value = "/**")
    public ResponseEntity<ErrorResponse> onNotSupportedMethod() {
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).body(
                new ErrorResponse(HttpStatus.METHOD_NOT_ALLOWED.toString(),
                        HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> onException(Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(HttpStatus.NOT_FOUND.toString(), ex.getMessage()));
    }
}
