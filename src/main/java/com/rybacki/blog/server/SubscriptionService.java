package com.rybacki.blog.server;

import com.rybacki.blog.server.model.Notification;
import com.rybacki.blog.server.model.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubscriptionService {

    public static final String NOTIFICATION_SAMPLE_MESSAGE = "Sample message";

    private SubscriptionRepository subscriptionRepository;
    private NotificationServiceRestClient client;

    @Value("${notification.service.endpoint}")
    private String notificationServiceEndpointUrl;

    @Autowired
    public SubscriptionService(SubscriptionRepository subscriptionRepository,
                               NotificationServiceRestClient client) {
        this.subscriptionRepository = subscriptionRepository;
        this.client = client;
    }

    @Async
    public void sendSubscriptionAsync(String authorUserName) {
        List<Subscription> subscriptions = subscriptionRepository
          .findByAuthorUserNameEquals(authorUserName);

        if (!subscriptions.isEmpty()) {
            List<Notification> notifications = subscriptions.stream().map(Notification::new)
              .collect(Collectors.toList());
            client.sendNotificationAsync(notificationServiceEndpointUrl, notifications);
        }
    }
}
