package com.rybacki.blog.server;

import com.rybacki.blog.server.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class NotificationServiceRestClient {

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Async
    public void sendNotificationAsync(String url, List<Notification> notifications) {
        restTemplate.postForEntity(url, notifications, null);
    }

}
