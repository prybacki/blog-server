package com.rybacki.blog.server;

import com.rybacki.blog.server.model.ErrorResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

public class BlogControllerTest {

    private static final String TEST_EXCEPTION = "TEST_EXCEPTION";

    private BlogController sut;

    @Mock
    private BlogService blogService;

    @Before
    public void setup() {
        initMocks(this);
        sut = new BlogController(blogService);
    }

    @Test
    public void shouldReturnErrorResponseWhenExceptionIsOccurred() {
        Exception ex = new Exception(TEST_EXCEPTION);
        ResponseEntity<ErrorResponse> response = sut.onException(ex);

        assertEquals(HttpStatus.NOT_FOUND.toString(), response.getBody().getCode());
        assertEquals(TEST_EXCEPTION, response.getBody().getMessage());

    }

}
