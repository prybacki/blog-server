package com.rybacki.blog.server;

import com.rybacki.blog.server.model.Notification;
import com.rybacki.blog.server.model.Subscription;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class SubscriptionServiceTest {

    private static final String TEST_AUTHOR_USER_NAME = "TEST_AUTHOR_USER_NAME";

    private static final String TEST_USER_ID_1 = "TEST_USER_ID_1";
    private static final String TEST_USER_ID_2 = "TEST_USER_ID_2";
    private static final Subscription TEST_SUBSCRIPTION_1 = new Subscription(TEST_USER_ID_1,
            TEST_AUTHOR_USER_NAME);
    private static final Subscription TEST_SUBSCRIPTION_2 = new Subscription(TEST_USER_ID_2,
            TEST_AUTHOR_USER_NAME);
    private static final List<Subscription> TEST_SUBSCRIPTION_LIST = Arrays
            .asList(TEST_SUBSCRIPTION_1, TEST_SUBSCRIPTION_2);

    private SubscriptionService sut;

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @Mock
    private NotificationServiceRestClient client;

    @Before
    public void setup() {
        initMocks(this);
        sut = new SubscriptionService(subscriptionRepository, client);
    }

    @Test
    public void shouldEachSubscriptionsElementIsSent() {
        ArgumentCaptor<List<Notification>> notifications = ArgumentCaptor.forClass(List.class);
        when(subscriptionRepository.findByAuthorUserNameEquals(any()))
                .thenReturn(TEST_SUBSCRIPTION_LIST);

        sut.sendSubscriptionAsync(TEST_AUTHOR_USER_NAME);

        verify(client).sendNotificationAsync(any(), notifications.capture());
        assertEquals(TEST_SUBSCRIPTION_LIST.size(), notifications.getValue().size());
        assertTrue(isEquals(notifications.getValue(), TEST_SUBSCRIPTION_LIST));
    }

    @Test
    public void shouldNotSentNotificationWhenNotExistsAnySubscription() {
        when(subscriptionRepository.findByAuthorUserNameEquals(any()))
                .thenReturn(Collections.emptyList());

        sut.sendSubscriptionAsync(TEST_AUTHOR_USER_NAME);

        verify(client, times(0)).sendNotificationAsync(any(), any());
    }

    private boolean isEquals(List<Notification> notifications, List<Subscription> subscriptions) {
        List<String> subscriptionsUserIds = subscriptions.stream().map(Subscription::getUserId)
                .collect(Collectors.toList());
        List<String> notificationsUserIds = notifications.stream().map(Notification::getUserId)
                .collect(Collectors.toList());

        Collections.sort(subscriptionsUserIds);
        Collections.sort(notificationsUserIds);

        return subscriptionsUserIds.equals(notificationsUserIds);
    }
}
