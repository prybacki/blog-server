package com.rybacki.blog.server;

import com.rybacki.blog.server.model.BlogPost;
import com.rybacki.blog.server.model.Subscription;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class BlogServiceTest {

    private static final String TEST_CONTENT = "TEST_CONTENT";
    private static final String TEST_SUBJECT = "TEST_SUBJECT";
    private static final List<String> TEST_TAGS = Arrays.asList("TEST_TAG1", "TEST_TAG2");
    private static final LocalDate TEST_MODIFICATION_DATE = LocalDate.of(2019, 7, 22);
    private static final String TEST_AUTHOR_NAME = "TEST_AUTHOR_NAME";
    private static final String TEST_AUTHOR_USER_NAME = "TEST_AUTHOR_USER_NAME";
    private static final BlogPost TEST_BLOG_POST = new BlogPost(TEST_CONTENT, TEST_SUBJECT,
      TEST_TAGS, TEST_MODIFICATION_DATE, TEST_AUTHOR_NAME, TEST_AUTHOR_USER_NAME);

    private static final String TEST_USER_ID = "TEST_USER_ID";
    private static final Subscription TEST_SUBSCRIPTION = new Subscription(TEST_USER_ID,
      TEST_AUTHOR_USER_NAME);

    private static final Long TEST_ID = 1L;

    private BlogService sut;

    @Mock
    private BlogRepository blogRepository;

    @Mock
    private SubscriptionRepository subscriptionRepository;

    @Mock
    private SubscriptionService subscriptionService;

    @Before
    public void setup() {
        initMocks(this);
        sut = new BlogService(blogRepository, subscriptionRepository, subscriptionService);
    }

    @Test
    public void shouldReturnBlogPostWhenPostExist() {
        when(blogRepository.findById(any())).thenReturn(Optional.of(TEST_BLOG_POST));

        BlogPost blogPost = sut.findPostById(TEST_ID);

        assertEquals(TEST_BLOG_POST, blogPost);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalExceptionWhenPostNotExist() {
        when(blogRepository.findById(any())).thenReturn(Optional.empty());

        sut.findPostById(TEST_ID);
    }
}
