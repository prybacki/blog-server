package com.rybacki.blog.server.e2e;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rybacki.blog.server.model.BlogPost;
import com.rybacki.blog.server.model.BlogPostResponse;
import com.rybacki.blog.server.model.Subscription;
import com.rybacki.blog.server.model.SubscriptionResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import javax.sql.DataSource;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.anything;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withNoContent;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=create-drop"
})
public class End2EndTest {

    private static final String TEST_CONTENT = "TEST_CONTENT";
    private static final String TEST_SUBJECT = "TEST_SUBJECT";
    private static final List<String> TEST_TAGS = Arrays.asList("TEST_TAG1", "TEST_TAG2");
    private static final LocalDate TEST_MODIFICATION_DATE = LocalDate.of(2019, 7, 22);
    private static final String TEST_AUTHOR_NAME = "TEST_AUTHOR_NAME";
    private static final String TEST_AUTHOR_USER_NAME = "TEST_AUTHOR_USER_NAME";
    private static final BlogPost TEST_BLOG_POST = new BlogPost(TEST_CONTENT, TEST_SUBJECT,
            TEST_TAGS, TEST_MODIFICATION_DATE, TEST_AUTHOR_NAME, TEST_AUTHOR_USER_NAME);

    private static final String TEST_USER_ID = "TEST_USER_ID";
    private static final Subscription TEST_SUBSCRIPTION = new Subscription(TEST_USER_ID,
            TEST_AUTHOR_USER_NAME);

    private static final String TEST_BLOG_POST_RESPONSE_RAW = "'{'\"postId\":{0}'}'";
    private static final String TEST_SUBSCRIPTION_RESPONSE_RAW = "'{'\"subscriptionId\":{0}'}'";

    private static final String CREATE_POST_URL = "/posts";
    private static final String CREATE_SUBSCRIPTION_URL = "/subscriptions";
    private static final String DELETE_SUBSCRIPTION_URL = "/subscriptions/{0}";

    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private WebTestClient webClient;
    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        mockServer = MockRestServiceServer.createServer(restTemplate);
        jsonMapper = new ObjectMapper();
    }

    @After
    public void cleanup() {
        final List<String> databaseTablesToDelete = Arrays
                .asList("BLOG_POST_TAGS", "BLOG_POST", "SUBSCRIPTION");
        for (String table : databaseTablesToDelete) {
            try {
                JdbcTestUtils.deleteFromTables(jdbcTemplate, table);
            } catch (Exception ignored) {
            }
        }
    }

    @Test
    public void shouldPersitPostWhenPostsRequestIsSent() throws JsonProcessingException {

        BlogPostResponse response = createPost();

        assertEquals(1, getPostCount());
        assertNotNull(response.getPostId());
        assertEquals(MessageFormat.format(TEST_BLOG_POST_RESPONSE_RAW, response.getPostId()),
                jsonMapper.writeValueAsString(response));
    }

    @Test
    public void shouldPersitSubscriptionWhenSubscriptionRequestIsSent() throws JsonProcessingException {
        SubscriptionResponse response = createSubscription();

        assertEquals(1, getSubscritpionCount());
        assertNotNull(response.getSubscriptionId());
        assertEquals(
                MessageFormat.format(TEST_SUBSCRIPTION_RESPONSE_RAW, response.getSubscriptionId().toString()),
                jsonMapper.writeValueAsString(response));
    }

    @Test
    public void shouldSentNotificationWhenPostAndSubscriptionExist() throws InterruptedException {
        mockServer.expect(anything()).andExpect(method(HttpMethod.POST)).andRespond(withNoContent());

        createSubscription();
        createPost();

        //notifications are sent by separate thread, is needed additional time to verify it by mock server
        Thread.sleep(1000);
        mockServer.verify();
    }

    @Test
    public void shouldNotSentNotificationWhenSubscriptionIsDeleted() throws InterruptedException {
        mockServer.expect(ExpectedCount.never(), MockRestRequestMatchers.method(HttpMethod.POST));

        Long id = createSubscription().getSubscriptionId();
        deleteSubscription(id);
        createPost();

        //notifications are sent by separate thread, is needed additional time to verify it by mock server
        Thread.sleep(1000);
        mockServer.verify();
    }

    private BlogPostResponse createPost() {
        return webClient.post()
                .uri(CREATE_POST_URL)
                .body(Mono.just(TEST_BLOG_POST), BlogPost.class)
                .exchange().expectStatus().isOk()
                .expectBody(BlogPostResponse.class).returnResult().getResponseBody();
    }

    private int getSubscritpionCount() {
        return JdbcTestUtils.countRowsInTable(jdbcTemplate, "SUBSCRIPTION");
    }

    private SubscriptionResponse createSubscription() {
        return webClient.post()
                .uri(CREATE_SUBSCRIPTION_URL)
                .body(Mono.just(TEST_SUBSCRIPTION), Subscription.class)
                .exchange().expectStatus().isOk()
                .expectBody(SubscriptionResponse.class).returnResult().getResponseBody();
    }

    private int getPostCount() {
        return JdbcTestUtils.countRowsInTable(jdbcTemplate, "BLOG_POST");
    }

    private void deleteSubscription(Long id) {
        webClient.delete()
                .uri(MessageFormat.format(DELETE_SUBSCRIPTION_URL, id))
                .exchange();
    }

}

