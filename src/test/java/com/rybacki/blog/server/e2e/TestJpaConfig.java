package com.rybacki.blog.server.e2e;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EntityScan(basePackages = "com.rybacki.blog.server.model")
@PropertySource("classpath:test.application.properties")
@EnableTransactionManagement
public class TestJpaConfig {

    @Autowired
    private Environment env;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("test.datasource.driverClassName"));
        dataSource.setUrl(env.getProperty("test.datasource.url"));
        dataSource.setUsername(env.getProperty("test.datasource.username"));
        dataSource.setPassword(env.getProperty("test.datasource.password"));

        return dataSource;
    }

}
